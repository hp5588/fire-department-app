# Intro
App implemented in Java that helps fire department to reduce preparation time by providing light indication and warning sound. Run on Raspberry Pi which require pi4j lib for GPIO operation. Display (LCD2864) is attached to show count-down and event details. OTA Firmware update is also implemented to automatically upgrade to latest firmware. 
# Features
- support events logging on back-end server (https://gitlab.com/hp5588/fire-department-web.git)
- programmable LED blinking pattern/length/color in a config file
- support up to 5A current on self-designed PCB
- LCD2864 & Groove LCD are supported as information display
- OTA update

# Images 
![](images/image_1.jpg)
