/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.peripheral.define;

/**
 * Created by hp558 on 2017/1/18.
 */
public class GroveLcdControllerDef {


    public static final byte REG_RED = (byte) 0x04 ;       // pwm2
    public static final byte REG_GREEN = (byte) 0x03 ;    // pwm1
    public static final byte REG_BLUE = (byte) 0x02 ;      // pwm0

    public static final byte REG_GRPPWM = (byte) 0x06 ;
    public static final byte REG_GRPFREQ = (byte) 0x07 ;


    public static final byte REG_MODE1 = (byte) 0x00 ;
    public static final byte REG_MODE2 = (byte) 0x01 ;
    public static final byte REG_OUTPUT = (byte) 0x08 ;


    public static final byte LCD_READ_FLAG = (byte) 0x80;
    public static final byte LCD_WRITE_FLAG = (byte) 0xC0;


    // commands
    public static final byte LCD_CLEARDISPLAY = (byte) 0x01;
    public static final byte LCD_RETURNHOME  = (byte) 0x02;
    public static final byte LCD_ENTRYMODESET = (byte) 0x04;
    public static final byte LCD_DISPLAYCONTROL = (byte) 0x08;
    public static final byte LCD_CURSORSHIFT = (byte) 0x10 ;
    public static final byte LCD_FUNCTIONSET = (byte) 0x20  ;
    public static final byte LCD_SETDDRAMADDR = (byte) 0x80;
    public static final byte LCD_SETCGRAMADDR = (byte) 0x40  ;

// flags for display entry mode
    public static final byte LCD_ENTRYRIGHT = (byte) 0x00 ;
    public static final byte LCD_ENTRYLEFT = (byte) 0x02 ; 
    public static final byte LCD_ENTRYSHIFTINCREMENT = (byte) 0x01 ;
    public static final byte LCD_ENTRYSHIFTDECREMENT = (byte) 0x00 ;

// flags for display on/off control
    public static final byte LCD_DISPLAYON = (byte) 0x04 ; 
    public static final byte LCD_DISPLAYOFF = (byte) 0x00 ;
    public static final byte LCD_CURSORON = (byte) 0x02 ;  
    public static final byte LCD_CURSOROFF = (byte) 0x00 ; 
    public static final byte LCD_BLINKON = (byte) 0x01 ;   
    public static final byte LCD_BLINKOFF = (byte) 0x00 ;  

// flags for display/cursor shift
    public static final byte LCD_DISPLAYMOVE = (byte) 0x08 ;
    public static final byte LCD_CURSORMOVE = (byte) 0x00 ;
    public static final byte LCD_MOVERIGHT = (byte) 0x04 ; 
    public static final byte LCD_MOVELEFT = (byte) 0x00 ;  

// flags for function set
    public static final byte LCD_8BITMODE = (byte) 0x10 ;  
    public static final byte LCD_4BITMODE = (byte) 0x00 ;  
    public static final byte LCD_2LINE = (byte) 0x08 ;     
    public static final byte LCD_1LINE = (byte) 0x00 ;     
    public static final byte LCD_5x10DOTS = (byte) 0x04 ;  
    public static final byte LCD_5x8DOTS = (byte) 0x00 ;   
}
