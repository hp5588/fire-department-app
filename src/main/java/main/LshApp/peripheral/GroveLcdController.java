/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.peripheral;

import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import main.LshApp.Main;

import java.io.IOException;
import java.util.logging.Logger;

import static main.LshApp.peripheral.define.GroveLcdControllerDef.*;

/**
 * Created by hp558 on 2017/1/17.
 */
public class GroveLcdController {
    public enum Color{
        OFF,
        RED,
        GREEN,
        BLUE,
        WHITE
    }

    private boolean connected = false;

    private I2CDevice lcdDevice;
    private I2CDevice ledDevice;

    private Byte LED_ADDR = 0xc4 >> 1;
    private Byte LCD_ADDR = 0x7c >> 1;

    private byte displayControl;
    private int cursorLocation = 0;

    private Logger logger = Main.getMainLogger();
    private static String lcdLogHeader = "LCD :";

    public GroveLcdController() {
        try {
            I2CBus i2c = I2CFactory.getInstance(I2CBus.BUS_1);
            ledDevice = i2c.getDevice(LED_ADDR);
            lcdDevice = i2c.getDevice(LCD_ADDR);
            connected = true;
        } catch (I2CFactory.UnsupportedBusNumberException | IOException e) {
            connected = false;
            e.printStackTrace();
        }

        init();
    }

    private void init(){
        try {

            //wait for device to be ready
            Thread.sleep(50);

            /*Init LCD*/
            //setup LCD  (i2c #0x7c)
            byte functionOption = LCD_5x8DOTS | LCD_2LINE | LCD_8BITMODE;
            sendLcdCommand((byte) (LCD_FUNCTIONSET | functionOption));

            displayControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
            sendLcdCommand((byte) (LCD_DISPLAYCONTROL | displayControl));

            byte displayMode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
            sendLcdCommand((byte) (LCD_ENTRYMODESET | displayMode ));

            clear();
            home();

            /*init LED*/
            //setup LED controller (i2c #0x4c)
            setLedReg(REG_MODE1, (byte) 0);
            setLedReg(REG_MODE2, (byte) 0x20);
            setLedReg(REG_OUTPUT, (byte) 0xFF);
            ledBlinkOff();
            setColor(Color.WHITE);


        } catch (InterruptedException | IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]");
        }

    }
    public void setCursor(int col, int row){
        try {
            if (row==0){
                col |= 0x80;
            }else {
                col |= 0xc0; //( 0x80 | 0x40 ) second row start from 40H
            }
            cursorLocation = row*16+col;
            sendLcdCommand((byte) col);
        } catch (IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]");
        }
    }

    public void clear(){
        try {
            sendLcdCommand( LCD_CLEARDISPLAY );
            sleep(2);
        } catch (IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]");
        }

    }
    public void home(){
        try {
            sendLcdCommand( LCD_RETURNHOME );
            sleep(2);
        } catch (IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]");
        }
    }

    public void print(String string){
        try {
            for (int i = 0; i < string.length(); i++) {
                if (i == 16) {
                    setCursor(0, 1);
                }
                sendLcdData((byte) string.charAt(i));
            }
        } catch(IOException e){
            logOnLcdWriteFail(string);
        }
    }


    private void sleep(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sendLcdCommand(byte commandByte) throws IOException {
        byte[] command ={(byte)0x80,commandByte};
        lcdDevice.write(command);
    }

    private void sendLcdData(byte data) throws IOException {
        byte[] dataBytes ={(byte)0x40,data};
        lcdDevice.write(dataBytes);
    }

    private void setLedReg(byte address, byte data) throws IOException {
        ledDevice.write(new byte[]{address, data});
    }

    public void setColor(int r, int g, int b){
        try {
            setLedReg(REG_RED, (byte) r);
            setLedReg(REG_GREEN, (byte) g);
            setLedReg(REG_BLUE, (byte) b);
        } catch (IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]" + r +"," + g + "," + b);
        }
    }

    public void setColor(Color color){
        switch (color){
            case RED:{
                setColor(255,0,0);
                break;
            }
            case GREEN:{
                setColor(0,255,0);
                break;
            }
            case BLUE:{
                setColor(0,0,255);
                break;
            }
            case WHITE:{
                setColor(255,255,255);
                break;
            }
            case OFF:{
                setColor(0,0,0);
                break;
            }
        }
    }


    public void ledBlinkOn(int sec){
        try {
            setLedReg(REG_GRPFREQ, (byte) (sec*24-1));
            setLedReg(REG_GRPPWM, (byte) 0x7f);
        } catch (IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]");
        }
    }
    public void ledBlinkOff(){
        try {
            setLedReg(REG_GRPFREQ, (byte) 0x00);
            setLedReg(REG_GRPPWM, (byte) 0xff);
        } catch (IOException e) {
//            logOnLcdWriteFail("[" + this.getClass().getEnclosingMethod().getName()  + " failed]");
        }
    }

    private void logOnLcdWriteFail(String warningMsg){
        logger.warning( lcdLogHeader + warningMsg);
    }



}
