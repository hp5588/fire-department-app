/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.peripheral;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

/**
 * Created by hp558 on 2017/3/25.
 */
public class LED {
    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public boolean isReverse() {
        return reverse;
    }

    private boolean reverse;

    public GpioPinDigitalOutput getLedPinDigitalOutput() {
        return ledPinDigitalOutput;
    }

    private GpioPinDigitalOutput ledPinDigitalOutput;

    public LED(GpioPinDigitalOutput ledPinDigitalOutput ,boolean reverse) {
        this.ledPinDigitalOutput = ledPinDigitalOutput;
        this.reverse = reverse;
    }


}
