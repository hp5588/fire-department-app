/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.peripheral;

import com.pi4j.io.gpio.*;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import main.LshApp.Main;
import main.LshApp.peripheral.define.Lcd2864gDef;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hp558 on 2017/2/15.
 */
public class Lcd2864gController {

    private I2CDevice lcd;

    private int x;
    private int y;
    private int spaceSize = 1;
    /*row-major*/
    private byte[][] screenBuffer = new byte[Lcd2864gDef.SCR_HEIGHT][Lcd2864gDef.SCR_WIDTH];
    private byte[][] savedScreenBuffer = new byte[Lcd2864gDef.SCR_HEIGHT][Lcd2864gDef.SCR_WIDTH];

    private GpioController gpioController = Main.getGpioController();



    public Lcd2864gController() {
        init();
    }

    private void init(){
        try {
            I2CBus i2c = I2CFactory.getInstance(I2CBus.BUS_1);
            lcd = i2c.getDevice(Lcd2864gDef.DEVICE_ADDR);
        } catch (IOException | I2CFactory.UnsupportedBusNumberException e) {
            e.printStackTrace();
        }
        GpioPinDigitalOutput resetButton = gpioController.provisionDigitalOutputPin(RaspiPin.GPIO_01, PinState.LOW);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        resetButton.high();

        byte[] cmds = {
                (byte) 0x3b,
                (byte) 0x03,

                (byte) 0x38,
                (byte) 0x05,
                (byte) 0x0c,

                (byte) 0x39,
                (byte) 0x08,
                (byte) 0x10,
                (byte) 0x8c,

                (byte) 0x3A ,
                Lcd2864gDef.COMMAND_PARTIAL_DSP_MODE, //Disable
//                Lcd2864gDef.COMMAND_DSP_CTL | 0x01
        };

        command(cmds);



    }

    public int getMaxPageCount(){
        return Lcd2864gDef.PAGE_COUNT;
    }
    public int getMaxRowCharCount(){
        return Lcd2864gDef.ROW_CHAR_COUNT;
    }

    public void focus(){
        savedScreenBuffer = screenBuffer;
        clearStatusBar();
        clearBottomBar();
    }

    public void defocus(){
        screenBuffer = savedScreenBuffer;
    }

    public void clear(){
        screenBuffer = new byte[Lcd2864gDef.SCR_HEIGHT][Lcd2864gDef.SCR_WIDTH];
    }
    public void clearPage (int page){
        setPageBackground(page,false);
/*        for (int row = 0; row < Lcd2864gDef.CHAR_HEIGHT; row++) {
            screenBuffer[page*Lcd2864gDef.CHAR_HEIGHT + row] = new byte[Lcd2864gDef.SCR_WIDTH];
        }*/
    }
    public void clearRowRange(int begin, int end){


    }
    public void clearPlot(){
        for (int page = 0; page < Lcd2864gDef.PAGE_COUNT-2; page++) {
            clearPage(page+1);
        }
    }
    public void clearStatusBar(){
        clearPage(0);
    }

    public void clearBottomBar(){
        clearPage(Lcd2864gDef.PAGE_COUNT-1);
    }

    public void printHorizontalCenter(String string, int row){
        printHorizontalCenter(string,row,false);
    }
    public void printHorizontalCenter(String string, int row,boolean reverse){
        placeString(string, Lcd2864gDef.SCR_HORIZONTAL_CENTER-string.length()*Lcd2864gDef.CHAR_WIDTH/2, row * Lcd2864gDef.CHAR_HEIGHT,reverse);
    }

    public void printVerticalCenter(String string ,int leftOffset, Lcd2864gDef.HorizontalAlign horizontalAlign){
        int x, y;
        y = Lcd2864gDef.SCR_HEIGHT/2 - Lcd2864gDef.CHAR_HEIGHT/2;

        switch (horizontalAlign){
            case CENTER:{
                x = Lcd2864gDef.SCR_WIDTH - string.length() * Lcd2864gDef.CHAR_WIDTH / 2;
                break;
            }
            case RIGHT:{
                x = Lcd2864gDef.SCR_WIDTH - string.length() * Lcd2864gDef.CHAR_WIDTH - 5;
                break;
            }

            default:
            case LEFT:{
                x = 0;
                break;
            }
        }

        x += leftOffset;

        placeString(string,x,y);
    }
    public void print(String string, int row, int index){
        placeString(string, index*Lcd2864gDef.CHAR_WIDTH, row * Lcd2864gDef.CHAR_HEIGHT);
    }

    public void placeString(String string, int x, int y){
        placeString(string,x,y,false);
    }
    public void placeCenterString(String string, int y){
        placeCenterString(string,y,false);
//        placeString(string,Lcd2864gDef.SCR_HORIZONTAL_CENTER - string.length()*Lcd2864gDef.CHAR_WIDTH/2, y, false);
    }

    public void placeCenterString(String string, int y , boolean reverse){
        placeString(string,Lcd2864gDef.SCR_HORIZONTAL_CENTER - string.length()*Lcd2864gDef.CHAR_WIDTH/2, y, reverse);
    }

    public void printStatusBar(String string){
        setPageBackground(0,true);
        printHorizontalCenter(string,0,true);
    }

    public void printBottomBar(String string){
        clearBottomBar();
        printHorizontalCenter(string, Lcd2864gDef.PAGE_COUNT-1);
    }




    public void setPageBackground(int page, boolean reverse){

        for (int row = 0; row < Lcd2864gDef.CHAR_HEIGHT; row++) {
            byte[] filledDot = new byte[Lcd2864gDef.SCR_WIDTH];
            if (reverse){
                Arrays.fill(filledDot, (byte) 0x01);
            }
            screenBuffer[page*Lcd2864gDef.CHAR_HEIGHT + row] = filledDot;
        }
    }


    public void placeString(String string, int x, int y, boolean reverse){
        if (string==null)
            return;

        /*check over range*/
        if (x + string.length()*Lcd2864gDef.CHAR_WIDTH +1 > Lcd2864gDef.SCR_WIDTH){
            return;
        }
    /*    if (y+1 > Lcd2864gDef.PAGE_COUNT){
            return;
        }*/

        List<byte[]> bitmaps = stringToBitmaps(string);
        int colCount = 0;
        for (byte[] charByte :
                bitmaps) {
            for (byte colByte :
                    charByte) {
                if (reverse){
                    colByte = (byte) ~colByte;
                }
                for (int h = 0; h < Lcd2864gDef.CHAR_HEIGHT; h++) {
                    screenBuffer[y + h][x + colCount] = (byte) ((colByte >> h) & 0x01);
                }
                colCount++;
            }
            /*space between characters*/
            if (reverse){
                for (int h = 0; h < Lcd2864gDef.CHAR_HEIGHT; h++) {
                    screenBuffer[y + h][x + colCount] = (byte) 0x01;
                }
            }
            colCount++;
    }




   /*     byte colAddr = (byte) (x * Lcd2864gDef.CHAR_WIDTH);
        byte pageAddrCmd = (byte) (Lcd2864gDef.COMMAND_Y_ADDR & (y & 0x0f));
        byte colLAddrCmd = (byte) (Lcd2864gDef.COMMAND_X_L_ADDR & (colAddr & 0x0f));
        byte colHAddrCmd = (byte) (Lcd2864gDef.COMMAND_X_H_ADDR & ((colAddr>>4) & 0x0f));

        byte[] cmds = {
                pageAddrCmd,
                colLAddrCmd,
                colHAddrCmd
        };
        *//*set page and column start point before writing*//*
        command(cmds);


        *//*write data to display RAM*//*
        List<byte[]> bitmaps = stringToBitmaps(string);
        for (byte[] bitmap :
                bitmaps) {
            *//*TODO check if works*//*
            send(bitmap);
        }*/

        /*int endX = string.length() % Lcd2864gDef.ROW_CHAR_COUNT + x;
        int endY = y + (string.length() + x) / Lcd2864gDef.ROW_CHAR_COUNT;

        int col = x;
        for (int i = 0; i < endY-y; i++) {
            *//*print a page*//*

            *//*set print position*//*
            byte pageAddrCmd = (byte) (Lcd2864gDef.COMMAND_Y_ADDR & (y & 0x0f));
            byte colLAddrCmd = (byte) (Lcd2864gDef.COMMAND_X_L_ADDR & (col & 0x0f));
            byte colHAddrCmd = (byte) (Lcd2864gDef.COMMAND_X_H_ADDR & (stringEndX & 0x0f));

            for (int j = 0; j < ; j++) {

            }

        }

        int stringStartX =  x*Lcd2864gDef.CHAR_WIDTH;
        int stringStartY = y;
        int stringEndX = stringStartX + string.length() * Lcd2864gDef.CHAR_WIDTH;
        int stringEndY;


        byte pageAddr = (byte) (Lcd2864gDef.COMMAND_Y_ADDR & (y & 0x0f));
        byte colLAddr = (byte) (Lcd2864gDef.COMMAND_X_L_ADDR & (stringStartX & 0x0f));
        byte colHAddr = (byte) (Lcd2864gDef.COMMAND_X_H_ADDR & (stringEndX & 0x0f));
*/
    }

    public boolean placeCenterInfoBox(String info, int width, int height){
        int upperRow = Lcd2864gDef.SCR_HEIGHT/2 - height/2;
        int leftCol = Lcd2864gDef.SCR_WIDTH/2 - width/2;

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                screenBuffer[upperRow + row][leftCol + col] = 0x01;
            }
        }

        placeString(info, Lcd2864gDef.SCR_HORIZONTAL_CENTER - (info.length() * Lcd2864gDef.CHAR_WIDTH)/2, Lcd2864gDef.SCR_VERTICAL_CENTER-Lcd2864gDef.CHAR_HEIGHT/2,true);
//        placeString(info,58 , 32,true);
        return true;

    }


    /*pixel*/
    /*row-major image*/
    public boolean placeImage(byte[][] imageMatrix, int top, int left){
        if (imageMatrix==null){
            return false;
        }
        /*check overflow*/
        if (imageMatrix.length + top > Lcd2864gDef.SCR_HEIGHT ){
            return false;
        }
        if (imageMatrix[0].length + left > Lcd2864gDef.SCR_WIDTH){
            return false;
        }


        for (int imgRow = 0; imgRow < imageMatrix.length; imgRow++) {
            for (int imgCol = 0; imgCol < imageMatrix[0].length; imgCol++) {
                screenBuffer[top+imgRow][left+imgCol] = imageMatrix[imgRow][imgCol] ;
            }
        }

        return true;
    }

    public void plotEventImage(byte[][] imageMatrix){
        placeImage(imageMatrix, Lcd2864gDef.STATUS_BAR_HEIGHT, 5);
    }

    public static byte[][] zoomImg(byte[][] src, int magnification){
        if (src==null){
            return null;
        }

        byte[][] buf = new byte[src.length*magnification] [src[0].length*magnification];

        for (int rowCount = 0; rowCount < src.length; rowCount++) {
            byte[] row =src[rowCount];
            for (int colCount = 0; colCount < row.length; colCount++) {
                byte col = row[colCount];
                for (int magCountY = 0; magCountY < magnification; magCountY++) {
                    for (int magCountX = 0; magCountX < magnification; magCountX++) {
                        buf[rowCount*magnification + magCountY][colCount*magnification + magCountX] = col;
                    }
                }
            }
        }
        return buf;
    }

    public void update(){
        /*init address at start point*/
/*        byte[] cmds = {
            Lcd2864gDef.COMMAND_Y_ADDR,
            Lcd2864gDef.COMMAND_X_L_ADDR,
            Lcd2864gDef.COMMAND_X_H_ADDR
        };
        command(cmds);*/


        /*write all buf onto screen*/
        for (int pageCount = 0; pageCount < Lcd2864gDef.PAGE_COUNT; pageCount++) {
            /*put cursor back to beginning while write data to each row*/
            byte pageAddrCmd = (byte) (Lcd2864gDef.COMMAND_Y_ADDR | (pageCount & 0x0f));
            byte colLAddrCmd = Lcd2864gDef.COMMAND_X_L_ADDR ;
            byte colHAddrCmd = Lcd2864gDef.COMMAND_X_H_ADDR ;
            byte[] cmd= {0x38,pageAddrCmd, colLAddrCmd, colHAddrCmd};
            command(cmd);


            byte[] rowData = new byte[Lcd2864gDef.SCR_WIDTH];
            for (int colCount = 0; colCount < screenBuffer[0].length; colCount++) {
                /*send row data*/
                /*convert to one byte as column, this will drop gray scale detail*/
                byte colByte = 0x00;
                for (int i = 0; i < 8; i++) {
                    colByte |= (byte) ((screenBuffer[i + pageCount*8][colCount]) << i) ;
                }
                /*TODO remove later*/
/*
                if ((colCount == 0)|| (colCount ==127)) {
                    colByte |= 0x01;
                }
*/

                rowData[colCount] = colByte;
            }

            byte[] shiftedData = new byte[rowData.length + Lcd2864gDef.SCR_LEFT_PADDING];

            System.arraycopy(rowData,0,shiftedData,Lcd2864gDef.SCR_LEFT_PADDING,rowData.length);
            send(shiftedData);
//            send(rowData);

//            Logger.getLogger("Matrix").info(String.valueOf(Hex.encodeHex(rowData)));


            /* TODO remove later*/
/*            for (byte[] row :
                    screenBuffer) {
                Logger.getLogger("Matrix").info(Arrays.toString(row));
            }*/
        }
    }


    private void command (byte[] commands){
        byte[] dataBytes = new byte[commands.length+1];
        dataBytes[0] = Lcd2864gDef.COMMAND_CONTROL;
        System.arraycopy(commands,0,dataBytes,1,commands.length);
        write(dataBytes);
    }
    private void send(byte[] data){
        byte[] dataBytes = new byte[data.length+1];
        dataBytes[0] = Lcd2864gDef.COMMAND_DATA;
        System.arraycopy(data,0,dataBytes,1,data.length);
        write(dataBytes);
    }

    private void write (byte[] data){
        try {
            lcd.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
/*        for (byte aData : data) {
            try {
                lcd.write(aData);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    private List<byte[]> stringToBitmaps(String string){
        List<byte[]> map = new ArrayList<>();
        for (char character :
                string.toCharArray()) {
            byte[] charMap = Lcd2864gDef.font_5x8[((int)character)-32];
            map.add(charMap);
        }
        return map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }




}
