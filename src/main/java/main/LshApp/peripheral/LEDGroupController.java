/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.peripheral;

import com.pi4j.io.gpio.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by hp558 on 2017/1/15.
 */
public class LEDGroupController {


    private final GpioController gpio = GpioFactory.getInstance();

    private int ledToggleRate = 500;  //msec
    private Timer blinkingTimer;
    private List<LED> ledGroupList = new ArrayList<LED>();

    public LEDGroupController(Pin pin, boolean reverse) {
//        gpioPinDigitalOutputList.add(gpio.provisionDigitalOutputPin(led));
        this.add(pin, reverse);
        initState();
/*        try {
            LED = gpio.provisionDigitalOutputPin(led);
        }catch (Exception e){
            logger.log(Level.WARNING, e.toString());
        }

        LED.setState(PinState.HIGH);*/
    }

    private void initState(){
        for (LED led :
                ledGroupList) {
            if (led.isReverse()){
                led.getLedPinDigitalOutput().high();
            }else {
                led.getLedPinDigitalOutput().low();
            }
        }
    }


    public void add(Pin pin, boolean reverse){
        LED led = new LED(gpio.provisionDigitalOutputPin(pin),reverse);
        if (reverse){
            led.getLedPinDigitalOutput().high();
        }else {
            led.getLedPinDigitalOutput().low();
        }
        ledGroupList.add(led);
    }

    public void blink(){
        blink(true);
    }

    public void blink(boolean onAtStart) {

        blinkingTimer = new Timer();

        /*init state*/
        if (onAtStart){
            for (LED led :
                    ledGroupList) {
                led.getLedPinDigitalOutput().toggle();
            }
        }

        /*schedule blink*/
        blinkingTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (LED led :
                        ledGroupList) {
                    led.getLedPinDigitalOutput().toggle();
                }
            }
        },0,ledToggleRate);

    }


    public void stop(){
        if (blinkingTimer!=null){
            blinkingTimer.cancel();
            blinkingTimer.purge();
        }
        initState();
    }



    public int getLedToggleRate() {
        return ledToggleRate;
    }

    public void setLedToggleRate(int ledToggleRate) {
        this.ledToggleRate = ledToggleRate;
    }

}
