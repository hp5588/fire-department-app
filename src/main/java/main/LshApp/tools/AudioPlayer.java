/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.tools;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import main.LshApp.Main;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp558 on 2017/3/18.
 */
public class AudioPlayer {
    private Map<String,Thread> playerList = new HashMap<>();

    public AudioPlayer() {

    }

    public void add(String fileName){
        playerList.put(fileName,null);
    }

    public void play(String fileName){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    AdvancedPlayer player = new AdvancedPlayer( getClass().getResourceAsStream(fileName));
                    player.play();
                } catch (JavaLayerException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        if (!playerList.containsKey(fileName)){
            playerList.put(fileName,thread);
        }else if (playerList.get(fileName)==null){
            playerList.replace(fileName,thread);
        }
    }

    public void stop(String fileName){
        Thread thread = playerList.get(fileName);
        thread.stop();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        playerList.remove(fileName);

    }
    public void stopAll(){
        for (Map.Entry<String, Thread> entry :
                playerList.entrySet()) {
            entry.getValue().stop();
        }
        playerList.clear();
    }

}
