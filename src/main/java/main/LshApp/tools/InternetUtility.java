/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.tools;

import java.io.IOException;
import java.net.*;
import java.util.Enumeration;

/**
 * Created by hp558 on 2017/1/24.
 */
public class InternetUtility {

    public static boolean isReachableAt(String serverURL){
        try {
            final URL url = new URL(serverURL);
            final URLConnection conn = url.openConnection();
            conn.connect();
            return true;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean isServerReachable(){
       return isReachableAt("http://ks-machining.com");
    }

    public static boolean isConnectedToInternet(){
       return isReachableAt("http://google.com");
    }

    public static String getWlanIp(){


        String ip = "";

        Enumeration<NetworkInterface> interfaces = null;
        try {
            interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()){
                NetworkInterface inter = interfaces.nextElement();
                if (inter.getName().contains("eth") || inter.getName().contains("wlan")){

                }else {
                    break;
                }

                for (Enumeration addrs = inter.getInetAddresses(); addrs.hasMoreElements(); ) {
                    InetAddress addr = (InetAddress) addrs.nextElement();
                    if (addr instanceof Inet4Address && !addr.isLoopbackAddress()) {
                        return addr.toString().replace("/", "");
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

/*        while (ip.length()==0) {
            NetworkInterface inter = getInterface();
            for (Enumeration addrs = inter.getInetAddresses(); addrs.hasMoreElements(); ) {
                InetAddress addr = (InetAddress) addrs.nextElement();
                if (addr instanceof Inet4Address && !addr.isLoopbackAddress()) {
                    return addr.toString().replace("/", "");
                }
            }
        }*/
        return null;

    }

    public static String getMacAddress(){
        try {
             StringBuilder sb = new StringBuilder();
            for (byte b : getInterface().getHardwareAddress()){
                if (sb.length() > 0)
                    sb.append(':');
                sb.append(String.format("%02x", b));
            }
            return sb.toString();

        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static NetworkInterface getInterface(){
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()){
                NetworkInterface inter = interfaces.nextElement();
                if (inter.getName().contains("wlan")){
                    return inter;
                }else if (inter.getName().contains("eth")){
                    return inter;
                }

            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;
    }
}
