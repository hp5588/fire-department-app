/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.tools;

import main.LshApp.Main;

import java.io.IOException;

/**
 * Created by hp558 on 2017/1/22.
 */
public class VPNConnector implements Runnable{
    @Override
    public void run() {
        Process process = null;
        try {
            int count=0;
            while (!InternetUtility.isServerReachable()){
                if (count>10)
                    return;
                Thread.sleep(10*1000);
                count++;
            }
//            String cmd = "ssh -NfR 1234:localhost:22 brian@ks-machining.com -p 2222";
            String cmd = "sudo openvpn --config /etc/openvpn/client.ovpn";
            process =  Runtime.getRuntime().exec(cmd);
            Main.getMainLogger().info("VPN return code:"+String.valueOf(process.waitFor()));

        } catch (IOException | InterruptedException e) {
            process.destroy();
            e.printStackTrace();
        }
    }


}
