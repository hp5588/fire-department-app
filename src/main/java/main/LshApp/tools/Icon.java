/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.tools;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * Created by hp558 on 2017/3/25.
 */
public class Icon {
    private final String path =  "/icon/";

    private BufferedImage bufferedImage;
    public static Logger logger = Logger.getLogger("Test");



    public Icon(String fileName) {
//        File imgFile = new File(getClass().getResource(path+fileName).getFile());
//        logger.info("Path : "+getClass().getResource(path+fileName).getFile());
        InputStream imgStream = getClass().getResourceAsStream(path+fileName);
        try {
            bufferedImage = ImageIO.read(imgStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public byte[][] toByteMatrix(){
        if (bufferedImage==null){
            return null;
        }

        byte[][] imgMatrix = new byte[bufferedImage.getHeight()][bufferedImage.getWidth()];
        DataBufferByte dataBufferByte = ((DataBufferByte)bufferedImage.getRaster().getDataBuffer());
        int count = 0;
        for (byte dataByte :
                dataBufferByte.getData()){
            if (dataByte!=0) {
                imgMatrix[count / bufferedImage.getWidth()][count % bufferedImage.getWidth()] = 1;
            }
            count++;
        }
        return imgMatrix;
    }
}
