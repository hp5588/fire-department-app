/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.model;

import java.util.List;

/**
 * Created by hp558 on 2017/4/22.
 */
public class Event {
    public enum Color{
        Blue,
        Red
    }

    public enum Command{
        Blink,
        MutualBlink,
        Wait,
        Stop,
        PlayAudio
    }


    public Integer getPeriod() {
        return period;
    }

    public Command getCommand() {
        return command;
    }

    public List<Color> getLeds() {
        return leds;
    }

    public Integer getRate() {
        return rate;
    }

    public Integer getOffset() {
        return offset;
    }

    Integer offset;
    Integer period;
    Command command;
    List<Color> leds;
    Integer rate;

}
