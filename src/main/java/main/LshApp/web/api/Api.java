/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp.web.api;



import main.LshApp.tools.InternetUtility;
import main.LshApp.Main;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by hp558 on 2017/1/30.
 */
public class Api {

    private String url;
    private String user;
    private String uuid;

    private DataOutputStream wr;

    /*TODO remove later*/
    private Logger logger = Main.getMainLogger();

    private String firmwareUrl;
    private String firmwareMD5;
    private String firmwareVersion;

    public Api(String url, String user, String uuid) {
        this.url = url;
        this.user = user;
        this.uuid = uuid;
    }


    public void logEvent(String event){

        Map<String, String> values = new HashMap<String, String>();
        values.put("event_name",event);
        values.put("user",user);
        values.put("uuid",uuid);

        sendPOST("log",values);
    }

    private void connect(){
        try {
             URL obj = new URL(url);
             HttpURLConnection conn;

            URLConnection urlConnection = obj.openConnection();
            conn = (HttpURLConnection)  urlConnection;

            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded" );

            wr = new DataOutputStream(conn.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean hasUpdates(){

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!InternetUtility.isServerReachable()){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        try {
            thread.start();
            thread.join(20*1000);
            if (!downloadFirmwareInfo())
                return false;

            logger.info("latest firmware:" + firmwareVersion);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return !firmwareVersion.equals(Main.getVersion());

    }

    /*not returning if update success*/
    public boolean update(){

        downloadFirmwareInfo();

        String name = "lsh-app-"+ firmwareVersion;
        String tarName =  name +".tar";
        String workDir = "/";


        /*download file*/
        try {
            Path path = FileSystems.getDefault().getPath(workDir, tarName);
            URL website = new URL(firmwareUrl);
            InputStream in = website.openStream();
            long sizeCopied = Files.copy(in, path , StandardCopyOption.REPLACE_EXISTING);
            logger.info(sizeCopied + " byte downloaded");
        } catch (IOException e) {
            e.printStackTrace();
        }

        /* TODO verify md5*/
        try {

//            tar xf lsh-app-Beta1.1-3-gad461f6.tar && sudo cp -rT lsh-app-Beta1.1-3-gad461f6 lsh-app && rm -r lsh-app-Beta1.1-3-gad461f6 lsh-app-Beta1.1-3-gad461f6.tar
                /*extract and replace*/
                ProcessBuilder processBuilder =
                        new ProcessBuilder(
                                "bash", "-c",
                                "tar -xf " + tarName + "; "+
                                "cp " + "-rT " + name  + " lsh-app " + ";" +
                                "rm " + "-r " + name + " " + tarName
                                );

                processBuilder.directory(new File(workDir));
                Process process =  processBuilder.start();
                if (process.waitFor()!=0){
                    return false;
                }
        } catch (IOException | InterruptedException e) {
//            String errorInfo =org.apache.commons.io.IOUtils.toString(e.getErrorStream(), Charset.defaultCharset());
            logger.warning(e.getMessage());
            e.printStackTrace();
        }
        logger.info("update finish");
        return true;

    }

    private boolean downloadFirmwareInfo(){
        String result = sendGET("updates/check");

        try{
            JSONObject object = new JSONObject(result);
            firmwareVersion =  object.getString("version");
            firmwareMD5 = object.getString("md5");
            firmwareUrl = object.getString("url");
        }catch (Exception e){
            return false;
        }
        return true;
    }


    private String sendGET(String controller){
        return sendGET(controller,null);
    }
    private String sendGET(String controller, Map<String,String> values){
        StringBuilder result = new StringBuilder();

        try {
            URL obj = new URL(url + controller);
            HttpURLConnection conn;

            URLConnection urlConnection = obj.openConnection();
            conn = (HttpURLConnection)  urlConnection;
            conn.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    /*CREATE method in Rest Controller*/
    private void sendPOST(String controller, Map<String,String> values) {
        try {
            URL obj = new URL(url + controller);
            HttpURLConnection conn;

            URLConnection urlConnection = obj.openConnection();
            conn = (HttpURLConnection)  urlConnection;

            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            wr = new DataOutputStream(conn.getOutputStream());


            wr.writeBytes(mapToString(values));
            wr.flush();
            conn.getResponseCode();

            wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String mapToString(Map<String, String> values){
        String queryString = "";
        if (values!=null) {
            for (Map.Entry<String, String> entry :
                    values.entrySet()) {
                queryString += (entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        return queryString;
    }

    public void setUser(String user) {
        this.user = user;
    }
    public String getFirmwareUrl() {
        return firmwareUrl;
    }

    public String getFirmwareMD5() {
        return firmwareMD5;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }
}
