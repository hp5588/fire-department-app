/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp;


import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import main.LshApp.peripheral.GroveLcdController;
import main.LshApp.peripheral.LEDGroupController;
import main.LshApp.peripheral.Lcd2864gController;
import main.LshApp.peripheral.define.Lcd2864gDef;
import main.LshApp.tools.InternetUtility;
import main.LshApp.tools.VPNConnector;
import main.LshApp.web.api.Api;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.concurrent.Semaphore;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {

    private static final GpioController gpioController = GpioFactory.getInstance();

    private static Logger logger = Logger.getLogger("LSH");
    private static GroveLcdController groveLcdController = new GroveLcdController();

    private static Lcd2864gController lcdController = new Lcd2864gController();
    private static LEDGroupController sysLedGroupController = new LEDGroupController(RaspiPin.GPIO_25,false);

    protected static String macAddress;
    private static Api api;

    private static String args1 = "";
    private static String version;

    private static boolean DEBUG_MODE = false;
    private static String ip;

    public static void main(String[] args) {
        if (args.length>0) {
            args1 = args[0];
        }

        if (args1.equals("DB")){
            DEBUG_MODE = true;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH");
        Date date = new Date();
        String dateString  = dateFormat.format(date);
/*        try {
            PrintStream out  = new PrintStream(new FileOutputStream(dateString+ "-error.txt"));
            System.setErr(out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/

        try {
            FileHandler fileHandler = new FileHandler(dateString +"-log.txt");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
            logger.log(Level.INFO,"[Fire Department Advance Application] App Starting");
            fileHandler.flush();

//            lcdController.printHorizontalCenter("[Fire Department]",0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*get build version*/
        version = getVersionInfo();
        logger.info("Firmware Version: " + version);

        /*init LCD*/
        lcdController.placeCenterInfoBox("Connecting...", 100,40);
        lcdController.update();
/*        groveLcdController.print("Fire Department "+ ">>Connecting...");
        groveLcdController.setColor(GroveLcdController.Color.GREEN);*/

        /*get app uuid*/
        macAddress = InternetUtility.getMacAddress();
        logger.info("MAC: "+macAddress);

        /*init api*/
        api = new Api("http://ks-machining.com:3000/","Brian",macAddress);



        /*check if has new firmware*/

        /*TODO disable update temporary for new device*/

/*
        if (!args1.equals("DB")) {
            if (api.hasUpdates()) {
                logger.info("new firmware available");
                lcdController.clear();
                lcdController.placeCenterInfoBox("Updating>>>>>", 100, 50);
                lcdController.update();
 */
/*               groveLcdController.clear();
                groveLcdController.print("Updating firmware");*//*

                if (!api.update()) {
                    logger.warning("update failed");
                } else {
                */
/*restart service*//*

                    api.logEvent("Firmware Updated: " + api.getFirmwareVersion());
                    try {
                        Runtime.getRuntime().exec("sudo service lshd restart");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                logger.info("firmware up-to-date");
            }
        }else{
            logger.info("Update check skipped");
        }
*/


        /*VPN command*/
        Thread execThread = new Thread(new VPNConnector());
        execThread.start();



        EventManager eventManager = new EventManager();
        GpioPinDigitalInput ambulanceButton = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_00, PinPullResistance.PULL_UP);
        GpioPinDigitalInput fireButton = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_UP);
        GpioPinDigitalInput resetButton = gpioController.provisionDigitalInputPin(RaspiPin.GPIO_03, PinPullResistance.PULL_UP);


        Semaphore semaphore = new Semaphore(1);
        ambulanceButton.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                try {
                    semaphore.acquire();
                    if (!eventManager.isTriggered() && event.getEdge()==PinEdge.FALLING) {
                        logger.log(Level.INFO,"AMBULANCE event - " + "Started" );
                        eventManager.start(EventManager.EventType.AMBULANCE);
                    }
                    semaphore.release();
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                }
            }
        });
        fireButton.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                try {
                    semaphore.acquire();
                    if (!eventManager.isTriggered() && event.getEdge()==PinEdge.FALLING) {
                        logger.log(Level.INFO,"FIRE event - " + "Started" );
                        eventManager.start(EventManager.EventType.FIRE);
                    }
                    semaphore.release();
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                }
            }
        });
        resetButton.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                /*reset event*/
                try {
                    semaphore.acquire();
                    if (eventManager.isTriggered() && event.getEdge()==PinEdge.FALLING) {
                        logger.info("Canceled");
                        eventManager.reset();
                    }
                    semaphore.release();

                } catch (InterruptedException e) {
//                    e.printStackTrace();
                }
            }
        });



        /*new thread wait for ip*/
        Thread internetWatcher = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    boolean logged = false;
                    while (true){
                        if (!eventManager.isTriggered()) {
                            if (InternetUtility.isConnectedToInternet()) {
                                String ip = InternetUtility.getWlanIp();
                                if (ip != null) {
                                    Main.ip = ip;
                                    lcdController.printBottomBar(ip);
                                    lcdController.update();
                                    if (!logged) {
                                        logger.info("IP: " + ip);
                                        logged = true;
                                    }
                                }
                            } else {
                                Main.ip = null;
                                lcdController.printBottomBar("[Offline]");
                                lcdController.update();
                                logged = false;
                            }
                        }

                        Thread.sleep(3000);
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        internetWatcher.start();



        logger.log(Level.INFO,"App Ready");
        api.logEvent("App Ready");

        lcdController.clearPlot();
        lcdController.printHorizontalCenter("App Ready", Lcd2864gDef.PAGE_COUNT/2);
        lcdController.printStatusBar("Fire Department");

/*        Icon icon = new Icon("fire2.png");
        lcdController.placeImage(icon.toByteMatrix(),10,0);
        lcdController.update();*/

        /*blink system-up LED*/
        sysLedGroupController.setLedToggleRate(1000);
        sysLedGroupController.blink();

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                execThread.interrupt();
            }
        }));

        while (true){
            try {
                /*WTF...why this is affecting the stopping time of clip*/
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static String getVersionInfo() {
        Enumeration resEnum;
        try {
            resEnum = Thread.currentThread().getContextClassLoader().getResources(JarFile.MANIFEST_NAME);
            while (resEnum.hasMoreElements()) {
                try {
                    URL url = (URL)resEnum.nextElement();
                    InputStream is = url.openStream();
                    if (is != null) {
                        Manifest manifest = new Manifest(is);
                        Attributes mainAttribs = manifest.getMainAttributes();
                        String version = mainAttribs.getValue("version");
                        if(version != null) {
                            return version;
                        }
                    }
                }
                catch (Exception e) {
                    // Silently ignore wrong manifests on classpath?
                }
            }
        } catch (IOException e1) {
            // Silently ignore wrong manifests on classpath?
        }
        return null;
    }

    public static GroveLcdController getGroveLcdController(){
        return groveLcdController;
    }
    public static Logger getMainLogger(){return logger;}
    public static Api getApi() {
        return api;
    }
    public static String getVersion() {
        return version;
    }
    public static String getArgs1() {return args1;}

    public static LEDGroupController getSysLedGroupController() {
        return sysLedGroupController;
    }

    public static Lcd2864gController getLcdController() {
        return lcdController;
    }
    public static String getIp() {
        return ip;
    }
    public static GpioController getGpioController() {
        return gpioController;
    }



    public static boolean isDebugMode() {
        return DEBUG_MODE;
    }
}
