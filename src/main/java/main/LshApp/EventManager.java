/*
 * Copyright (c) 2017. Brian Kao
 * hp5588@gmail.com
 */

package main.LshApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import main.LshApp.model.Event;
import main.LshApp.peripheral.LEDGroupController;
import main.LshApp.peripheral.Lcd2864gController;
import main.LshApp.peripheral.define.Lcd2864gDef;
import main.LshApp.tools.AudioPlayer;
import main.LshApp.tools.Icon;
import main.LshApp.web.api.Api;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by hp558 on 2017/1/14.
 */
public class EventManager {

    public enum EventType {
        UNDEFINED,
        AMBULANCE,
        FIRE
    }

    /*constants*/
    private static final Pin LED_RED_PIN = RaspiPin.GPIO_04;
    private static final Pin LED_BLUE_PIN = RaspiPin.GPIO_05;
    private static final int LED_TOGGLE_RATE_DEFAULT = 250;
    private static final int LED_TOGGLE_RATE_FAST = 100;

    private Logger logger = Logger.getLogger("Event Manager");
    private boolean triggered = false;
    private int state = 0;
    private int time = 0;

    private EventType eventType = EventType.UNDEFINED;
    private List<Integer> eventLengthList = new ArrayList();
    private List<Timer> timerList = new ArrayList();
    private List<Event> events = new ArrayList<>();
    private JSONObject configJSONObject;

    private LEDGroupController blueLedGroupController = new LEDGroupController(LED_BLUE_PIN, true);
    private LEDGroupController redLedGroupController = new LEDGroupController(LED_RED_PIN, true);

    private Lcd2864gController lcdController = Main.getLcdController();

    private AudioPlayer player = new AudioPlayer();
    private String audioAt70Name = "/sound/" + "70sec.mp3";
    private String audioAt120Name = "/sound/" + "120sec.mp3";
    private JSONArray jsonArray = null;
    private ObjectMapper mapper = new ObjectMapper();


    private Api api = Main.getApi();

    EventManager (){
//        read setting parameters from config file
        try {
            String jsonString;
            if (Main.isDebugMode()){
                jsonString = new String(Files.readAllBytes(Paths.get("../config/configD")));
            }else {
                jsonString = new String(Files.readAllBytes(Paths.get("../config/config")));
            }
            logger.info("config file loaded:" + jsonString);
            configJSONObject= new JSONObject(jsonString);
        } catch (IOException e) {
            logger.warning("config load fail");
            e.printStackTrace();
        }

        blueLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_DEFAULT);
        redLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_DEFAULT);
        blueLedGroupController.add(RaspiPin.GPIO_26, false);
        redLedGroupController.add(RaspiPin.GPIO_27, false);

        Main.getSysLedGroupController().stop();

    }

    void start(EventType eventType){
        if (eventType == EventType.UNDEFINED){
            logger.log(Level.WARNING,"event type not set");
            return;
        }

        if(isTriggered()){
            return;
        }
        this.setTriggered(true);
        this.eventType = eventType;

        //choose time list to use
        Icon eventIcon = null;
        switch (eventType){
            case AMBULANCE:{
                jsonArray = configJSONObject.getJSONObject("ambulance").getJSONArray("events");
                eventIcon = new Icon("cross.png");
                break;
            }
            case FIRE:{
                jsonArray = configJSONObject.getJSONObject("fire").getJSONArray("events");
                eventIcon = new Icon("fire.png");

                break;
            }
            case UNDEFINED:
                break;
        }

        assert eventIcon != null;


        assert jsonArray != null;
        int timeAccumulated = 0;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonEventObject  = jsonArray.getJSONObject(i);
            Event event = null;
            try {
              event = mapper.readValue(jsonEventObject.toString(), Event.class);
              events.add(event);
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Integer period = Integer.parseInt(jsonEventObject.getJSONObject("period").toString());
            timeAccumulated += event.getPeriod();
            eventLengthList.add(timeAccumulated);
        }

        logger.info(eventLengthList.toString());



        Timer globalTimer = new Timer();


        lcdController.focus();
        lcdController.clearPlot();

        lcdController.plotEventImage(eventIcon.toByteMatrix());

        changeState();
        globalTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                count();
//                lcdController.placeCenterInfoBox( String.valueOf(time),48,40);
                lcdController.printVerticalCenter(String.valueOf(time) + " of "+ eventLengthList.get(eventLengthList.size()-1),-5, Lcd2864gDef.HorizontalAlign.RIGHT);
                lcdController.update();

//                check if play voice
                if (eventType == EventType.FIRE) {
                    if (time == eventLengthList.get(0) - 4) {
                        player.play(audioAt70Name);
                    } else if (time == eventLengthList.get(3) - 12) {
                        player.play(audioAt120Name);
                    }
                }
//                change state at specific moment
               if (eventLengthList.contains(time)){
                   changeState();
               }
            }
        },0,1000);
        timerList.add(globalTimer);


        //log using web API
        Thread logThread = new Thread(new Runnable() {
            @Override
            public void run() {
                api.logEvent(eventType.toString());
            }
        });
        logThread.start();



    }

    private int count(){
        return time++;
    }


    private void changeState(){
        state++;
        logger.log(Level.INFO, "State changed to "+ state);
        Event currentEvent = null;
        if (state <= eventLengthList.size()){
            currentEvent = events.get(state-1);
        }else {
            reset(false);
            return;
        }



        blueLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_DEFAULT);
        redLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_DEFAULT);


//        get command
/*        JSONObject currentEventObject = jsonArray.getJSONObject(state-1);
        String command = String.valueOf(currentEventObject.getJSONObject("command"));*/

        Integer rate = currentEvent.getRate();
//        logger.info("Rate: "+ currentEvent.getCommand());
        if (rate!=null){
            if (rate>0) {
                blueLedGroupController.setLedToggleRate(rate);
                redLedGroupController.setLedToggleRate(rate);
            }
        }

        switch (currentEvent.getCommand()){

            case Wait:{
                /*do nothing*/
                break;
            }
            case Blink:{
                List<Event.Color> ledColors = currentEvent.getLeds();
                boolean blueFlag = false;
                boolean redFlag = false;

                for (Event.Color color :
                        ledColors) {
                    switch (color){
                        case Blue:{
                            blueFlag = true;
                            break;
                        }
                        case Red:
                        {
                            redFlag = true;
                            break;
                        }
                    }
                }

                if (blueFlag)
                    blueLedGroupController.blink();
                else
                    blueLedGroupController.stop();

                if (redFlag)
                    redLedGroupController.blink();
                else
                    redLedGroupController.stop();



                break;
            }
            case MutualBlink:{
                blueLedGroupController.stop();
                redLedGroupController.stop();
                blueLedGroupController.blink(true);
                redLedGroupController.blink(false);
                break;
            }
            case PlayAudio:{

                break;
            }
            case Stop:{
                redLedGroupController.stop();
                blueLedGroupController.stop();
                break;
            }
            default:{
//                exit
                reset(false);
            }
        }



        /*
        switch (event){
            case AMBULANCE: {
                switch (state) {
                    case 0:
                        break;
                    case 1: {
                        blueLedGroupController.blink();
                        break;
                    }
                    case 2: {
                        blueLedGroupController.stop();
                        redLedGroupController.blink();
                        break;
                    }
                    case 3: {
                        reset(false);
                        Main.getSysLedGroupController().blink();

                        break;
                    }
                    default: {
                        reset();
                        logger.log(Level.WARNING, "Unhandled State");
                    }
                }
            }
            break;

            case FIRE:{
                switch (state) {
                    case 0:
                        break;
                    case 1:
                        break;

                    case 2: {
                        blueLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_FAST);
                        redLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_FAST);
                        blueLedGroupController.blink(true);
                        redLedGroupController.blink(false);
                        break;
                    }
                    case 3: {
                        redLedGroupController.stop();
                        blueLedGroupController.stop();
                        break;
                    }
                    case 4: {
                        blueLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_FAST);
                        redLedGroupController.setLedToggleRate(LED_TOGGLE_RATE_FAST);
                        blueLedGroupController.blink(true);
                        redLedGroupController.blink(false);
                        break;
                    }
                    case 5: {
                        reset(false);
                        Main.getSysLedGroupController().blink();
                        break;
                    }
                    default: {
                        reset();
                        logger.log(Level.WARNING, "Unhandled State");
                    }
                }
            }
            break;
        }*/


    }

    public void reset(){
        reset(true);
    }

    public void reset(boolean resetAudio){
        /*globalTimer.cancel();*/
        /*cancel all timer*/
        for (Timer timer :
                timerList) {
            timer.cancel();
//            timer.purge();
        }
        timerList.clear();

        if (resetAudio){
            player.stopAll();
        }
        redLedGroupController.stop();
        blueLedGroupController.stop();
        eventLengthList.clear();
        events.clear();


        showLastMission();

        this.setTriggered(false);
        eventType = EventType.UNDEFINED;
        state = 0;
        time = 0;

        logger.info("Reseated");


    }

    private void showLastMission(){

        lcdController.clear();
        lcdController.printBottomBar(Main.getIp());
        lcdController.printStatusBar("Fire Department");

        lcdController.printHorizontalCenter("Last Event: " + eventType.toString(),3);
        lcdController.printHorizontalCenter("At " +  new SimpleDateFormat("hh:mm:ss a").format(new Date()),4);
        lcdController.update();
    }






    public boolean isTriggered() {
        return triggered;
    }

    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }


    public EventType getEventType() {
        return eventType;
    }
}
